# powershell_cours_cesi_gmsi13_avril_2019
cls

#region création de la fonction
function afficher_le_contenu{
    param($chemin_du_fichier)
    $contenu = gc $chemin_du_fichier
    foreach ($item in $contenu) {
        write-host $item
    }
}
#endregion

# Utilisation de la fonction
$global:scriptpath = gi $MyInvocation.MyCommand.Definition
$readme_path = join-path $scriptpath.Directory.fullname "README.md"
$xml_path = join-path $scriptpath.Directory.fullname "exemple.xml"

#démonstration de l'utilisation d'une fonction
afficher_le_contenu($readme_path)

#Utilisation d'un contenu XML
[xml]$my_xml = gc $xml_path
write-host $my_xml.parametres.GetAttribute("update_time")
foreach ($node in $my_xml.parametres.ChildNodes ) {
    write-host $node.GetAttribute("nom")
}

try {
    
}
catch {
    
}