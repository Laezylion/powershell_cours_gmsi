cls
[xml]$XmlContent = gc ".\Parametrage.xml"
$Number = $XmlContent.Parametres.NumeroCandidat
$N = ""
1..(3 - $Number.length) |%{ $N = $N + "0" }
$NomMachine = "SRVAD" + $N + $Number
$IP = "10.10.$Number.10"

$MaskBits = 24 # This means subnet mask = 255.255.255.0
$Gateway = "10.10.$Number.1"
$Dns = "10.10.$Number.10"
$IPType = "IPv4"

write-host "Activation du verr Num au démarrage"
Set-ItemProperty -Path 'Registry::HKCU\Control Panel\Keyboard' -Name "InitialKeyboardIndicators" -Value "2"
$wsh = New-Object -ComObject WScript.Shell
$wsh.SendKeys('{NUMLOCK}')

write-host "Activation de Windows Update"
$AUSettigns = (New-Object -com "Microsoft.Update.AutoUpdate").Settings
$AUSettigns.NotificationLevel = 4
$AUSettigns.Save()


Write-host "Reconfiguration IP : $IP"

# Retrieve the network adapter that you want to configure
$adapter = Get-NetAdapter | ? {$_.Status -eq "up"}
# Remove any existing IP, gateway from our ipv4 adapter
If (($adapter | Get-NetIPConfiguration).IPv4Address.IPAddress) {
	$adapter | Remove-NetIPAddress -AddressFamily $IPType -Confirm:$false |Out-Null
}
If (($adapter | Get-NetIPConfiguration).Ipv4DefaultGateway) {
	$adapter | Remove-NetRoute -AddressFamily $IPType -Confirm:$false |Out-Null
}
#  Configure the IP address and default gateway
$adapter | New-NetIPAddress -AddressFamily $IPType -IPAddress $IP -PrefixLength $MaskBits -DefaultGateway $Gateway  

# Configure the DNS client server IP addresses
$adapter | Set-DnsClientServerAddress -ServerAddresses $DNS |Out-Null

$NomMachine |out-file ".\$NomMachine.log" -Append

Write-host "Reconfiguration du nom de machine : $NomMachine"
Rename-Computer -NewName $NomMachine -Restart