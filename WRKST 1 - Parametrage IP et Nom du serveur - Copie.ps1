cls
[xml]$XmlContent = gc ".\Parametrage.xml"
$Number = $XmlContent.Parametres.NumeroCandidat
$N = ""
1..(3 - $Number.length) |%{ $N = $N + "0" }
$NomMachine = "WRKST" + $N + $Number

$NomMachine |out-file ".\$NomMachine.log" -Append

Rename-Computer -NewName $NomMachine -Restart