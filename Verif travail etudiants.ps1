clear-host
#region variables de bases
    $ErrorActionPreference = "STOP"
    $global:scriptpath = get-item $MyInvocation.MyCommand.Definition
    $global:scriptname = $scriptpath.BaseName
    $xmlpath = join-path $scriptpath.Directory.fullname "$scriptname.xml"
    write-host "Controle des projets des etudiants" -BackgroundColor green
#endregion
[xml]$xml = get-content $xmlpath
#$commands_to_find =  "write-host","get-command","get-help","read-host"
$param = $xml.Parametres.ChildNodes |Where-Object { $_.name -eq "cmdlets_to_find" }
$commands_to_find = $param.InnerText.ToLower().split(",")

foreach ($node in $xml.Parametres.students.ChildNodes) {
    $git_project_name = $node.'#text'.split("/")[-1]
    $gitpullpath = join-path $scriptpath.Directory.fullname $git_project_name
    #region git pull
    $InstallerArgs = @(
        "pull"
    )
    Start-Process -FilePath "C:\Program Files\Git\cmd\git.exe" -ArgumentList $InstallerArgs -Wait -NoNewWindow  -WorkingDirectory $gitpullpath | Out-Null
    #endregion
}
clear-host
foreach ($node in $xml.Parametres.students.ChildNodes) {
    #region Définition du nom de projet, clone, et verification du dossier
        write-host  $node.GetAttribute("nom") -ForegroundColor Blue -NoNewline
        $nrc = 0
        $ratio = 0
        $git_project_name = $node.'#text'.split("/")[-1]
        if ($git_project_name -ne $node.GetAttribute("nom")) {
            $nrc ++
            #write-host "    Non respect des consignes : nom du projet gitlab : $git_project_name" -ForegroundColor DarkYellow
        }
        $CamelCase
        $LaValiseAmaGrandMere

        $snake_case
        $la_valise_a_ma_grand_mere


        $gitpath = join-path $scriptpath.Directory.fullname $git_project_name
        if(![System.IO.Directory]::Exists($gitpath)){
            $InstallerArgs = @(
                "clone"
                $node.'#text'
            )
            Start-Process -FilePath "C:\Program Files\Git\cmd\git.exe" -ArgumentList $InstallerArgs -Wait -NoNewWindow
        }
        if(![System.IO.Directory]::Exists($gitpath)){
            #write-host -ForegroundColor red "Impossible de cloner !"
            #exit
        }else{
    #endregion
            $hash=@{}
            foreach ($fichier in Get-ChildItem -Path $gitpath -Filter "*.ps1" -Recurse  ) {
                #write-host $fichier
                $content = get-content $fichier.FullName
                foreach ($command in $commands_to_find) {
                    foreach ($ligne in $content) {
                        if($ligne.ToLower().contains("$command")){
                            $hash["$command"] = "found"
                            break
                        }
                    }
                }
            }
            [double]$ratio = $hash.keys.count / $commands_to_find.count
        #region fin de boucles
        }
        write-host " $nrc" -ForegroundColor yellow -NoNewline
        write-host " $ratio" -ForegroundColor darkGreen  -NoNewline

        switch ($true) {
            ($ratio -gt 0.75) { write-host " A" -ForegroundColor green ;break }
            ($ratio -gt 0.50) { write-host " B" -ForegroundColor yellow ;break }
            ($ratio -gt 0.25) { write-host " C" -ForegroundColor RED ;break }
            Default {write-host " D" -ForegroundColor darkRED}
        }

    #endregion
}