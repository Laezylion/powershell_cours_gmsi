cls
[xml]$XmlContent = gc ".\Parametrage.xml"
$Number = $XmlContent.Parametres.NumeroCandidat
$ErrorActionPreference = "stop"

Write-Host "Cr�ation de la structure"

New-ADOrganizationalUnit -Name "_E$Number" -Path "DC=Entreprise$Number,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Comptes Utilisateurs" -Path "OU=_E$Number,DC=Entreprise$Number,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Groupes Utilisateurs" -Path "OU=_E$Number,DC=Entreprise$Number,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Comptes Ordinateurs" -Path "OU=_E$Number,DC=Entreprise$Number,DC=local" -ProtectedFromAccidentalDeletion $false

Write-host "Cr�ation des OU des Services"

$ListeUsers = Import-CSV ".\services_database.csv" 
$ListeUsers | foreach {
    Write-host "Cr�ation du service" $_.Service
    New-ADOrganizationalUnit -Name $_.Service -Path "OU=Comptes Utilisateurs,OU=_E$Number,DC=Entreprise$Number,DC=local" -ProtectedFromAccidentalDeletion $false
}

Write-host "Cr�ation des comptes users"

$ListeUsers = Import-CSV ".\users_database.csv" 
$ListeUsers | foreach {
    $OU=""
    $OU = $_.Service
    Write-host "Cr�ation du compte de " $_.FirstName $_.LastName
    New-ADUser -SamAccountName $_.Login `
    -Name ($_.FirstName + " " + $_.LastName) `
    -DisplayName ($_.FirstName + " " + $_.LastName)  `
    -Surname $_.LastName `
    -GivenName $_.FirstName `
    -Path "OU=$OU,OU=Comptes Utilisateurs,OU=_E$Number,DC=Entreprise$Number,DC=local"  `
    -AccountPassword (ConvertTo-SecureString -AsPlainText "P@sw0rd" -Force)  `
    -Enabled $true 
}


