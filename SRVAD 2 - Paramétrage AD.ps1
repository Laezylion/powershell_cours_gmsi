cls
[xml]$XmlContent = gc ".\Parametrage.xml"
$Number = $XmlContent.Parametres.NumeroCandidat
$ADName = "Entreprise$Number"
$ADNameFull = $ADName + ".local"
write-host "Cr�ation du compte admin de la nouvelle AD"
$creds = get-credential -UserName "$ADName\Administrator" -Message "Le mot de passe du compte administrateur ?"

write-host "Installation des composants AD DS"
Install-windowsfeature -name AD-Domain-Services -IncludeManagementTools

write-host "Inmportation des outils AD DS"
Import-Module ADDSDeployment

write-host "Cr�ation du domaine"

Install-ADDSForest `
-CreateDnsDelegation:$false `
-DomainMode "Win201" `
-DomainName $ADNameFull `
-DomainNetbiosName $ADName `
-ForestMode "Win2012R2" `
-InstallDns:$true `
-NoRebootOnCompletion:$false `
-SysvolPath "C:\Windows\SYSVOL" `
-DatabasePath "C:\Windows\NTDS" `
-LogPath "C:\Windows\NTDS" `
-Force:$true


exit
Install-ADDSDomainController `
    -DomainName "Entreprise_$Number.local" `
    -Credential $creds 

exit
Install-ADDSDomain `
    -Credential (Get-Credential CORP\EnterpriseAdmin1) `
    -NewDomainName $ADName `
    -ParentDomainName local `
    -InstallDNS -CreateDNSDelegation `
    -DomainMode Win2012R2 `
    -ReplicationSourceDC DC1.corp.contoso.com `
    -SiteName Houston `
    -DatabasePath "C:\Windows\NTDS" `
    -SYSVOLPath "C:\Windows\SYSVOL" `
    -LogPath "C:\Windows\Logs" `
    -NoRebootOnCompletion:$false


exit
Install-AddsDomain -Credential $creds`
 -DomainMode "Win2012R2" `
 -NewDomainName $ADName`
 -DomainNetbiosName $ADNameFull`
 -ForestMode "Win2012R2" `
 -InstallDns:$true -CreateDnsDelegation:$false`
 -NoRebootOnCompletion:$false `
 -SysvolPath "C:\Windows\SYSVOL" `
 -DatabasePath "C:\Windows\NTDS" `
 -LogPath "C:\Windows\NTDS" `
 -Force:$true


#$pass = get-
#