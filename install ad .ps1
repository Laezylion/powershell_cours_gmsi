$ADNameFull = "Stoef.local"
$ADName = "STO"

Install-ADDSForest `
-CreateDnsDelegation:$false`
-DomainMode "WinThreshold" `
-DomainName $ADNameFull `
-DomainNetbiosName $ADName `
-ForestMode "WinThreshold" `
-InstallDns:$true `
-NoRebootOnCompletion:$false `
-SysvolPath "C:\Windows\SYSVOL" `
-DatabasePath "C:\Windows\NTDS" `
-LogPath "C:\Windows\NTDS" `
-Force:$true