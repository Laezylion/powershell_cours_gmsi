cls
[xml]$XmlContent = gc ".\Parametrage.xml"
$Number = $XmlContent.Parametres.NumeroCandidat
$ErrorActionPreference = "stop"

Write-host "Cr�ation des dossiers des Services"

$ListeServices = Import-CSV ".\services_database.csv" 
$ListeServices | foreach {
    $ServiceName = $_.Service
    Write-host "Cr�ation du service $ServiceName"
    new-item -path "D:\E8\Srvs\$ServiceName" -ItemType directory -ErrorAction SilentlyContinue | Out-Null
}
Write-host ""

Write-host "Cr�ation des dossiers des Users"

$ListeUsers = Import-CSV ".\users_database.csv" 
$ListeUsers | foreach {
    $Login = $_.Login
    Write-host "Cr�ation du service $Login"
    new-item -path "D:\E8\Usrs\$Login" -ItemType directory -ErrorAction SilentlyContinue | Out-Null
}
Write-host ""