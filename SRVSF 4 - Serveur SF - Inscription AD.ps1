cls
[xml]$XmlContent = gc ".\Parametrage.xml"
$Number = $XmlContent.Parametres.NumeroCandidat
$ErrorActionPreference = "stop"

Write-Host "Inscription de ce serveur dans l'AD"
$domain = "Entreprise$Number.local"
$password = Read-Host -Prompt "Mot de passe Admin AD ?" -AsSecureString
$username = "$domain\Administrator"
$credential = New-Object System.Management.Automation.PSCredential($username,$password) 
Add-Computer -DomainName $domain -Credential $credential